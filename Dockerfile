FROM jenkins

USER root

RUN echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee /etc/apt/sources.list.d/webupd8team-java.list && echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee -a /etc/apt/sources.list.d/wepupd8team-java.list && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886 && echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections 

RUN apt-get update && apt-get install -y oracle-java8-installer mercurial unzip psmisc  && curl -L https://services.gradle.org/distributions/gradle-2.3-all.zip --output /opt/gradle-2.3-all.zip && unzip -u /opt/gradle-2.3-all.zip -d /opt/ && apt-get install -f -y

ENV GRADLE_HOME /opt/gradle-2.3

ENV PATH ${PATH} ${PATH}:/opt/gradle-2.3/bin

USER jenkins

